<?php

/**
 * @file
 * Drush command hooks for the cache_tools module.
 */

/**
 * Implements hook_drush_cache_clear().
 */
function cache_tools_drush_cache_clear(&$types) {

  // @TODO: Remove after https://drupal.org/node/2086633 is committed.
  if (module_exists('entitycache')) {
    $entities = entitycache_supported_core_entities(TRUE);
    foreach (array_keys($entities) as $type) {
      // You can't pass paramters to the callbacks in $types, so create an
      // anonymous function for each specific bin.
      $lamdba = create_function('', "return cache_clear_all('*', 'cache_entity_" . $type . "', TRUE);");
      $types['entitycache-' . str_replace('_', '-', $type)] = $lamdba;
    }
  }

  $types['module-implements'] = 'cache_tools_flush_module_implements';
  $types['hook-info'] = 'cache_tools_flush_hook_info';
  $types['bootstrap-modules'] = 'cache_tools_flush_bootstrap_modules';
}

/**
 * Clear the bootstrap_modules cache.
 */
function cache_tools_flush_bootstrap_modules() {
  cache_clear_all('bootstrap_modules', 'cache_bootstrap');
}

/**
 * Clear the hook_info cache.
 */
function cache_tools_flush_hook_info() {
  cache_clear_all('hook_info', 'cache_bootstrap');
}

/**
 * Clear the module_implements cache.
 */
function cache_tools_flush_module_implements() {
  cache_clear_all('module_implements', 'cache_bootstrap');
}

/**
 * Implements drush_hook_COMMAND_pre_validate().
 */
function drush_cache_tools_cache_command_clear_pre_validate($args) {
  if (variable_get('cache_tools_local_developer_override', FALSE)) {
    return;
  }

  $args = drush_get_arguments();
  if (isset($args[1]) && $args[1] == 'all') {
    drush_log(dt('Disabled by cache_tools, use multi-step drush cc instead.'), 'warning');
    return FALSE;
  }
}

/*
cache_bootstrap
bootstrap_modules
lookup_cache
schema
variables
common.inc:6079:function drupal_render_cache_set(&$markup, $elements) {
menu.inc:        cache_set($cid, $tree_parameters, 'cache_menu');
menu.inc:    cache_set($tree_cid, $data, 'cache_menu');
module.inc:      cache_set('bootstrap_modules', $bootstrap_list, 'cache_bootstrap');
module.inc:      cache_set('system_list', $lists, 'cache_bootstrap');
module.inc:    cache_set('module_implements', array(), 'cache_bootstrap');
module.inc:      cache_set('hook_info', $hook_info, 'cache_bootstrap');
path.inc:      cache_set($cid, $data, 'cache_path', $expire);
block/block.module:          cache_set($cid, $array, 'cache_block', CACHE_TEMPORARY);
field/field.info.inc:      cache_set("field_info_types:$langcode", $info, 'cache_field');
field/field.info.class.inc:    cache_set('field_info:field_map', $map, 'cache_field');
field/field.info.class.inc:      cache_set('field_info:fields', $this->fieldsById, 'cache_field');
field/field.info.class.inc:        cache_set('field_info:instances', $this->bundleInstances, 'cache_field');
field/field.info.class.inc:    cache_set("field_info:bundle:$entity_type:$bundle", $cache, 'cache_field');
field/field.info.class.inc:    cache_set("field_info:bundle_extra:$entity_type:$bundle", $info, 'cache_field');
field/field.attach.inc:        cache_set($cid, $data, 'cache_field');
node/node.module:  cache_set($cid, $_node_types);
locale/locale.module:        cache_set('locale:' . $langcode, $locale_t[$langcode]);
image/image.module:      cache_set('image_styles', $styles);
filter/filter.module:      cache_set("filter_formats:{$language->language}", $formats['all']);
filter/filter.module:      cache_set('filter_list_format', $filters['all']);
filter/filter.module:    cache_set($cache_id, $text, 'cache_filter');
*/
